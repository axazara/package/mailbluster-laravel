<?php

// config for AxaZara/MailBluster
return [
    'api_url' => env('MAILBLUSTER_API_URL', 'https://api.mailbluster.com/api'),
    'api_key' => env('MAILBLUSTER_API_KEY', 'default'),
];
