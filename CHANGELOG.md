# Changelog

All notable changes to `mailbluster-laravel` will be documented in this file.


## 1.0.1 - 2022-12-1
- Update README.md
- Update docs/Leads.md
- Update docs/Fields.md
- Update docs/Products.md

## 1.0.0 - 2022-11-10
- Initial release